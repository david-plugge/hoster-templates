#!/bin/sh
if [ ! -f "minecraft_server.jar" ]; then
    export SERVER_URL="https://api.modpacks.ch/public/modpack/${PACK_ID}/${PACK_VERSION}/server/linux"
    export INSTALL_FILE="serverinstall_${PACK_ID}_${PACK_VERSION}"

    # download server
    wget ${SERVER_URL} -O ${INSTALL_FILE} && chmod u+x ${INSTALL_FILE}
    # install server
    ./${INSTALL_FILE} --auto --noscript
    echo "eula=${EULA}" > eula.txt

    # rename server file
    for file in forge*.jar ; do
        mv "$file" "minecraft_server.jar"
    done
fi

java -XX:+UseG1GC -XX:+UnlockExperimentalVMOptions -Xmx${RAM} -Xms${RAM_RESERVED} -jar minecraft_server.jar nogui/app