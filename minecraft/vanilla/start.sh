#!/bin/sh
if [ ! -f "minecraft_server.jar" ]; then
    export SERVER_URL="https://launcher.mojang.com/v1/objects/a16d67e5807f57fc4e550299cf20226194497dc2/server.jar"

    # download server
    wget ${SERVER_URL} -O "minecraft_server.jar"
    # install server
    echo "eula=${EULA}" > eula.txt
fi

java -Xmx${RAM} -Xms${RAM_RESERVED} -jar minecraft_server.jar nogui